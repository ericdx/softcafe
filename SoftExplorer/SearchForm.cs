﻿using FormsLibrary;
using FSBrowser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Elenco
{
    public partial class SearchForm : Form
    {
        public SearchForm()
        {
            InitializeComponent();
            lbFound.Items.AddRange(SoftLibra.unsorted.Take(15).ToArray());
        }

        private void tbFind_KeyPress(object sender, KeyPressEventArgs e)
        {
           lbFound.Items.refill(SoftLibra.lookup(tbFind.Text));
        }
    }
}
