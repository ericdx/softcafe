﻿/*
 * Created by SharpDevelop.
 * User: Лилия
 * Date: 10.07.2019
 * Time: 01:00
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace SoftExplorer
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.ListBox lbLog;
		private System.Windows.Forms.Button btnQuit;
		public System.Windows.Forms.Button btnRescan;
		
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			/*if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);*/
			Hide();
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		public void InitializeComponent()
		{
            this.lbLog = new System.Windows.Forms.ListBox();
            this.btnQuit = new System.Windows.Forms.Button();
            this.btnRescan = new System.Windows.Forms.Button();
            this.btnCatalog = new System.Windows.Forms.Button();
            this.btnMissing = new System.Windows.Forms.Button();
            this.tbKey = new System.Windows.Forms.TextBox();
            this.btnApiKey = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CBVirusOff = new System.Windows.Forms.CheckBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbLog
            // 
            this.lbLog.FormattingEnabled = true;
            this.lbLog.Location = new System.Drawing.Point(10, 28);
            this.lbLog.Margin = new System.Windows.Forms.Padding(2);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(326, 225);
            this.lbLog.TabIndex = 2;
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(353, 29);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(107, 23);
            this.btnQuit.TabIndex = 3;
            this.btnQuit.Text = "Quit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.BtnQuitClick);
            // 
            // btnRescan
            // 
            this.btnRescan.Location = new System.Drawing.Point(353, 87);
            this.btnRescan.Name = "btnRescan";
            this.btnRescan.Size = new System.Drawing.Size(107, 23);
            this.btnRescan.TabIndex = 5;
            this.btnRescan.Text = "Full rescan";
            this.btnRescan.UseVisualStyleBackColor = true;
            this.btnRescan.Click += new System.EventHandler(this.BtnRescanClick);
            // 
            // btnCatalog
            // 
            this.btnCatalog.Location = new System.Drawing.Point(353, 59);
            this.btnCatalog.Name = "btnCatalog";
            this.btnCatalog.Size = new System.Drawing.Size(107, 23);
            this.btnCatalog.TabIndex = 6;
            this.btnCatalog.Text = "Edit catalog";
            this.btnCatalog.UseVisualStyleBackColor = true;
            this.btnCatalog.Click += new System.EventHandler(this.btnCatalog_Click);
            // 
            // btnMissing
            // 
            this.btnMissing.Location = new System.Drawing.Point(353, 116);
            this.btnMissing.Name = "btnMissing";
            this.btnMissing.Size = new System.Drawing.Size(107, 23);
            this.btnMissing.TabIndex = 7;
            this.btnMissing.Text = "Clear missing";
            this.btnMissing.UseVisualStyleBackColor = true;
            this.btnMissing.Click += new System.EventHandler(this.btnMissing_Click);
            // 
            // tbKey
            // 
            this.tbKey.Location = new System.Drawing.Point(6, 19);
            this.tbKey.Name = "tbKey";
            this.tbKey.Size = new System.Drawing.Size(214, 20);
            this.tbKey.TabIndex = 8;
            this.tbKey.Text = "paste your key";
            // 
            // btnApiKey
            // 
            this.btnApiKey.Location = new System.Drawing.Point(145, 45);
            this.btnApiKey.Name = "btnApiKey";
            this.btnApiKey.Size = new System.Drawing.Size(75, 23);
            this.btnApiKey.TabIndex = 9;
            this.btnApiKey.Text = "save api key";
            this.btnApiKey.UseVisualStyleBackColor = true;
            this.btnApiKey.Click += new System.EventHandler(this.btnApiKey_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(7, 55);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(79, 13);
            this.linkLabel1.TabIndex = 10;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "obtain your key";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.CBVirusOff);
            this.groupBox1.Controls.Add(this.tbKey);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Controls.Add(this.btnApiKey);
            this.groupBox1.Location = new System.Drawing.Point(480, 29);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(228, 100);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "VirusTotal";
            // 
            // CBVirusOff
            // 
            this.CBVirusOff.AutoSize = true;
            this.CBVirusOff.Location = new System.Drawing.Point(140, 74);
            this.CBVirusOff.Name = "CBVirusOff";
            this.CBVirusOff.Size = new System.Drawing.Size(92, 17);
            this.CBVirusOff.TabIndex = 11;
            this.CBVirusOff.Text = "disable check";
            this.CBVirusOff.UseVisualStyleBackColor = true;
            this.CBVirusOff.CheckedChanged += new System.EventHandler(this.CBVirusOff_CheckedChanged);
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(353, 146);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(107, 23);
            this.btnStop.TabIndex = 12;
            this.btnStop.Text = "Stop scanning";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(480, 136);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Press Ctrl+Alt+F to find apps";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 259);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnMissing);
            this.Controls.Add(this.btnCatalog);
            this.Controls.Add(this.btnRescan);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.lbLog);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

        private System.Windows.Forms.Button btnCatalog;
        private System.Windows.Forms.Button btnMissing;
        private System.Windows.Forms.TextBox tbKey;
        private System.Windows.Forms.Button btnApiKey;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox CBVirusOff;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label1;
    }
}
