﻿/*
 * Created by SharpDevelop.

 */
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Elenco;
using FSBrowser;
using TextLib;

namespace SoftExplorer
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
        EditAppsForm frmEdit;
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
            string key;
            Settings.fromFile("key", out key);
            tbKey.Text = key;            
		}
		void BtnQuitClick(object sender, EventArgs e)
		{
			Application.Exit();
		}
       

        public bool scan_started { get; internal set; }

        void BtnRescanClick(object sender, EventArgs e)
		{          

            NotificationIcon.rescan();
            btnRescan.Text = "Stop";
            btnRescan.Click -= new System.EventHandler(this.BtnRescanClick);
btnRescan.Click += new System.EventHandler(this.btnStop_Click);
        }

        private void btnCatalog_Click(object sender, EventArgs e)
        {
                            frmEdit = new EditAppsForm();
            frmEdit.Show();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            Visor.stopSignal = true;
        }

        internal void log(string msg)
        {
            lbLog.Items.Add(msg);
        }

        private void btnMissing_Click(object sender, EventArgs e)
        {
            Visor.clear();
            NotificationIcon.reproduceMenu(); 
        }

        private void btnApiKey_Click(object sender, EventArgs e)
        {
            File.WriteAllText("key",tbKey.Text);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.virustotal.com/gui/join-us");
        }

        private void CBVirusOff_CheckedChanged(object sender, EventArgs e)
        {
            var cb = sender as CheckBox;
            Settings.set("kav_off", cb.Checked);
        }

        private void btnStop_Click_1(object sender, EventArgs e)
        {
            Visor.stopSignal = true;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (scan_started)
                btnStop.Enabled = true;
        }
    }
}
