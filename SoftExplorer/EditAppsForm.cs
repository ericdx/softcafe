﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using FormsLibrary;
using System.Windows.Forms;
using FSBrowser;
using SoftExplorer;

namespace Elenco
{
    public partial class EditAppsForm : Form
    {
        public EditAppsForm()
        {
            InitializeComponent();
            lbCatalog.Items.AddRange(SoftLibra.getCategories());
            lbUnsorted.Items.AddRange(SoftLibra.unsorted);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SoftLibra.catalog.saveCatalog();
        }
        
        private void btnAdd_Click(object sender, EventArgs e)
        {
            lbCatalog.Items.Add(tbAdd.Text);
            SoftLibra.catalog.addCategory(tbAdd.Text);
        }

        private void EditAppsForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && lbUnsorted.SelectedItem != null
                )
            {
                var categ = SoftLibra.catalog.assignFolder(lbUnsorted.SelectedItem, lbCatalog.SelectedItem,int.Parse(tbDepth.Text));
                if (categ == null)
                    return;//root not allowed
                if (lbCatalog.SelectedItem == null)
                    lbCatalog.Items.Add(categ);
                 lbUnsorted.Items.removeItemsContaining(categ);
            }
        }

        private void EditAppsForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void lbCatalog_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                lbInside.Items.Clear();
                lbInside.Items.AddRange(SoftLibra.catalog.getAssigned(lbCatalog.SelectedItem));
            }
            else
            {
               //delete
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRebuild_Click(object sender, EventArgs e)
        {
            NotificationIcon.restoreMenu();
        }

        private void btnSubmenu_Click(object sender, EventArgs e)
        {
            lbCatalog.Items.Insert(lbCatalog.SelectedIndex,"---" + tbAdd.Text);
            SoftLibra.catalog.addCategory(tbAdd.Text,true,lbCatalog.SelectedItem as string);
        }
    }
}
