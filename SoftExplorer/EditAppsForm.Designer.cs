﻿namespace Elenco
{
    partial class EditAppsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbUnsorted = new System.Windows.Forms.ListBox();
            this.lbInside = new System.Windows.Forms.ListBox();
            this.lbCatalog = new System.Windows.Forms.ListBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.tbAdd = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDepth = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRebuild = new System.Windows.Forms.Button();
            this.btnSubmenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbUnsorted
            // 
            this.lbUnsorted.FormattingEnabled = true;
            this.lbUnsorted.Location = new System.Drawing.Point(12, 50);
            this.lbUnsorted.Name = "lbUnsorted";
            this.lbUnsorted.Size = new System.Drawing.Size(482, 160);
            this.lbUnsorted.TabIndex = 0;
            this.lbUnsorted.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EditAppsForm_MouseDown);
            // 
            // lbInside
            // 
            this.lbInside.FormattingEnabled = true;
            this.lbInside.Location = new System.Drawing.Point(12, 216);
            this.lbInside.Name = "lbInside";
            this.lbInside.Size = new System.Drawing.Size(482, 160);
            this.lbInside.TabIndex = 0;
            // 
            // lbCatalog
            // 
            this.lbCatalog.FormattingEnabled = true;
            this.lbCatalog.Location = new System.Drawing.Point(534, 99);
            this.lbCatalog.Name = "lbCatalog";
            this.lbCatalog.Size = new System.Drawing.Size(169, 277);
            this.lbCatalog.TabIndex = 1;
            this.lbCatalog.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbCatalog_MouseDown);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(534, 383);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tbAdd
            // 
            this.tbAdd.Location = new System.Drawing.Point(534, 32);
            this.tbAdd.Name = "tbAdd";
            this.tbAdd.Size = new System.Drawing.Size(100, 20);
            this.tbAdd.TabIndex = 3;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(641, 32);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(62, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Assign to category with RMB click";
            // 
            // tbDepth
            // 
            this.tbDepth.Location = new System.Drawing.Point(184, 24);
            this.tbDepth.Name = "tbDepth";
            this.tbDepth.Size = new System.Drawing.Size(31, 20);
            this.tbDepth.TabIndex = 6;
            this.tbDepth.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "levels from root";
            // 
            // btnRebuild
            // 
            this.btnRebuild.Location = new System.Drawing.Point(615, 382);
            this.btnRebuild.Name = "btnRebuild";
            this.btnRebuild.Size = new System.Drawing.Size(75, 23);
            this.btnRebuild.TabIndex = 8;
            this.btnRebuild.Text = "Rebuild";
            this.btnRebuild.UseVisualStyleBackColor = true;
            this.btnRebuild.Click += new System.EventHandler(this.btnRebuild_Click);
            // 
            // btnSubmenu
            // 
            this.btnSubmenu.Enabled = false;
            this.btnSubmenu.Location = new System.Drawing.Point(588, 58);
            this.btnSubmenu.Name = "btnSubmenu";
            this.btnSubmenu.Size = new System.Drawing.Size(115, 23);
            this.btnSubmenu.TabIndex = 9;
            this.btnSubmenu.Text = "Add submenu";
            this.btnSubmenu.UseVisualStyleBackColor = true;
            this.btnSubmenu.Click += new System.EventHandler(this.btnSubmenu_Click);
            // 
            // EditAppsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 450);
            this.Controls.Add(this.btnSubmenu);
            this.Controls.Add(this.btnRebuild);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbDepth);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tbAdd);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lbCatalog);
            this.Controls.Add(this.lbInside);
            this.Controls.Add(this.lbUnsorted);
            this.Name = "EditAppsForm";
            this.Text = "EditApps";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditAppsForm_FormClosing);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.EditAppsForm_MouseDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbUnsorted;
        private System.Windows.Forms.ListBox lbInside;
        private System.Windows.Forms.ListBox lbCatalog;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox tbAdd;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbDepth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRebuild;
        private System.Windows.Forms.Button btnSubmenu;
    }
}