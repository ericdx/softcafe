﻿/*
 * Created by SharpDevelop.

 */
using Elenco;
using FSBrowser;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Input;
using GlobalHotKey;
using System.Windows.Forms;
using FormsLibrary;
using NetLibrary;
using TextLib;

namespace SoftExplorer
{
    public class NotificationIcon
    {
        private NotifyIcon notifyIcon;
       
        private WinMenu softMenu;
        static MainForm mainFrm { get; set; }
        static Loading loadingFrm;
        #region Initialize icon and menu
        public NotificationIcon()
        {
            notifyIcon = new NotifyIcon();
            softMenu = new WinMenu();

            softMenu.Opening += MainMenu_Opening;
            softMenu.Closing += MainMenu_Closing;

            notifyIcon.Click += IconClick;

            notifyIcon.Icon = new Icon("icon.ico");
            notifyIcon.ContextMenuStrip = softMenu;
        }

        private void MainMenu_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            if (bw != null)
                bw.WorkerReportsProgress = true;
        }

        private void MainMenu_Opening(object sender, CancelEventArgs e)
        {
            if (bw != null)
                bw.WorkerReportsProgress = false;
        }
        #endregion
        static FSBrowser.Visor visor;
        static NotificationIcon notificationIcon;
        static BackgroundWorker bw;

        public static void restoreMenu()
        {
            mainFrm.log("rebuilding menu..");
            var menu = produceMenu();
            rebuildMenu(menu);
        }

        #region Main - Program entry point
        /// <summary>Program entry point.</summary>
        /// <param name="args">Command Line Arguments</param>
        [STAThread]
        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool isFirstInstance;
            // Please use a unique name for the mutex to prevent conflicts with other programs
            using (Mutex mtx = new Mutex(true, "elencoSoftExplorer33", out isFirstInstance))
            {
                if (isFirstInstance)
                {
 loadingFrm = new Loading();
                    loadingFrm.Show();
                    notificationIcon = new NotificationIcon();
                    mainFrm = new MainForm();

                    if (SoftLibra.tryRestore())
                    {
                        restoreMenu();
                        
                    }
                    else
                    {                        
                        rescan();
                    }
loadingFrm.Hide();
                    notificationIcon.notifyIcon.Visible = true;
                   var hotKeyManager = new HotKeyManager();

                    // Register Ctrl+Alt+F5 hotkey. Save this variable somewhere for the further unregistering.
                    var hotKey = hotKeyManager.Register(Key.F, ModifierKeys.Control | ModifierKeys.Alt);

                    // Handle hotkey presses.
                    hotKeyManager.KeyPressed += HotKeyManagerPressed;
                    Application.Run();
                    notificationIcon.notifyIcon.Dispose();
                }
                else
                {
                    // The application is already running
                    // TODO: Display message box or change focus to existing application instance
                    MessageBox.Show("Running!");
                }
            } // releases the Mutex
        }

       static SearchForm findFrm;
        /// <summary>
        /// open search form alt ctrl F
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void HotKeyManagerPressed(object sender, KeyPressedEventArgs e)
        {
            if (e.HotKey.Key == Key.F)
            {
                if (findFrm?.IsDisposed ?? true)
                    findFrm = new SearchForm();
                findFrm.Show();
            }
        }

        public static void rescan()
        {
           Visor.stopSignal = false;
            mainFrm.scan_started = true;
            mainFrm.log("scan started");
            bw = new BackgroundWorker();
            bw.WorkerReportsProgress = true;
            bw.DoWork += new DoWorkEventHandler(buildMenu);
            bw.ProgressChanged += new ProgressChangedEventHandler(menuPortion);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(menuBuilt);
            bw.RunWorkerAsync();
        }

        static public void reproduceMenu()
        {
            var m = produceMenu();
            rebuildMenu(m);
        }

        static void rebuildMenu(ToolStripMenuItem[] toolStripMenuItems)
        {
            notificationIcon.softMenu.Items.Clear();
            notificationIcon.softMenu.Items.AddRange(toolStripMenuItems);
            mainFrm.log("menu created.");
        }

        static void menuBuilt(object sender,
            RunWorkerCompletedEventArgs e)
        {
            rebuildMenu((ToolStripMenuItem[])
                                         e.Result);

            SoftLibra.flush();
            if (Visor.stopSignal)  mainFrm.log("stopped on demand");
        else mainFrm.log("scan finished");
            mainFrm.btnRescan.Text = "Full rescan";
            mainFrm.scan_started = false;
        }
        static void menuPortion(object sender,
            ProgressChangedEventArgs e)
        {
            notificationIcon.softMenu.Items.Clear();
            notificationIcon.softMenu.Items.AddRange((ToolStripMenuItem[])e.UserState);
        }

        static ToolStripMenuItem[] produceMenu()
        {
            var categorySubmenu = new HashSet<ToolStripMenuItem>();
            var uncategorized = new HashSet<ToolStripMenuItem>();
            foreach (var shortFolder in SoftLibra.mainMenu)
            {
                //folders
                var folderExes = SoftLibra.allSoft[shortFolder];
                if (folderExes == null) continue;
                var vfolder = folderExes[0].folderFS;
                var mi = vfolder.Contains(Visor.unsortedFolder)
                    ? new ToolStripMenuItem(vfolder, null)
                    : new ToolStripMenuItem(vfolder, null, openExplorer);
                mi.DropDownItems.AddRange(folderExes.Select(e =>
                        {
                            //fill exes
                            var mit = new ToolStripMenuItem(e.appName, null, executor);

                            mit.Tag = e.fullExePath;
                            return mit;
                        }
                    ).ToArray());
                var subMenu = folderExes[0].catalogOrFolder;
                if (subMenu.isCategory)
                {
                    if (!categorySubmenu.Any(s => s.Text == subMenu.category))
                    {
                        var categMenu = new ToolStripMenuItem(subMenu.category, null);
                        categMenu.BackColor = Color.Bisque;
                        categorySubmenu.Add(categMenu);

                    }
                    categorySubmenu.Single(s => s.Text == subMenu.category).DropDownItems.Add(mi);
                }
                else uncategorized.Add(mi);
            }
            return categorySubmenu.Concat(uncategorized).ToArray();
        }

        static void buildMenu(object sender,
            DoWorkEventArgs evt)
        {
            visor = new FSBrowser.Visor();
            string[] folders// =  //ConfigurationSettings.AppSettings["scanDirs"].Split(new char[] {';'},StringSplitOptions.RemoveEmptyEntries);
                = System.IO.DriveInfo.GetDrives().Select(drv => drv.RootDirectory.FullName).ToArray();
            ToolStripMenuItem[] menu;

            do
            {
                folders = visor.FindNewSoft(folders);//.Take(3).ToArray());

                menu = produceMenu();

                if (bw.WorkerReportsProgress)
                    ((BackgroundWorker)sender).ReportProgress(10, menu);
            }
            while (folders.Length > 0 && !Visor.stopSignal);
            evt.Result = menu;
        }
        #endregion

        #region Event Handlers
        private void menuAboutClick(object sender, EventArgs e)
        {
            MessageBox.Show("About This Application");
        }

        private static void openExplorer(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            System.Diagnostics.Process.Start(item.Text);
        }
        private static void executor(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            try
            {
                var fl = item.Tag as string;
                bool alarm;
                string key = null;
                var disabled = bool.Parse(Settings.getFile("kav_off"));
                if (!disabled && ! Settings.fromFile("key", out key))
                {
                    MessageBox.Show("you did not set api key in settings", "oops", MessageBoxButtons.OK);
                    return;
                }
                if (!disabled) {
                    var scan = new TotalScan(key);
                    if (!scan.checkFile(fl, out alarm))
                    {
                        if (alarm)
                        {
                            MessageBox.Show("virus alert. Execution cancelled.", "alarm", MessageBoxButtons.OK);
                            return;
                        }
                        var res = MessageBox.Show("unable to check .exe for malware. Proceed execution ?", "virus check", MessageBoxButtons.YesNo);
                        if (res == DialogResult.No)
                            return;
                    } 
                }
                System.Diagnostics.Process.Start(fl);
            }
            catch (Win32Exception) { } //if user cancelled 
        }
        private void menuExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void IconClick(object sender, EventArgs e)
        {
            var mouse = e as MouseEventArgs;
            if (mouse.Button == MouseButtons.Left)
                if (mainFrm != null)
                {
                    mainFrm.Show(); 

                    mainFrm.WindowState = FormWindowState.Normal;
                }
        }
        #endregion
    }
}
