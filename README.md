# SoftCafe

A handy tray app to find & launch apps on your pc, all including portable. 

new feature 1-dec-19: virustotal check before app launch, totally online!

Usage: 
1. At startup, it scans all your drives for .exe & builds a context menu bound to the tray icon. 
You choose to run apps or open their folders from the menu with lmb or rmb.
2. You have early access to the menu built at the moment.
3. When finished, it saves configuration on disk for quick startup next time.
4. You have an option to scan again your drives for new apps.
5. You can edit categories in settings to group apps & folders. 
6. Global hotkey to find an executable quickly
Todo list:
 semantic grouping for large subfolder list (for ex. Program files)
 recommended soft to setup with choco

Latest release: [download](https://1drv.ms/u/s!Am4FBRsRsoeugoIfoQIQkOo4McI_nQ)

Dependant libraries are here: https://gitlab.com/ericdx/libra . 

Any feedback is appreciated ericdx@rambler.ru
