﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonLibrary;
using ClassLib;
using TextLib;

namespace FSBrowser
{
    public class Catalog
    {
        public Dictionary<Category, LibItems> items { get; set; } //= new Dictionary<Category, LibItems>();
        public string catalogFile => "catalog";

        public string commonCat => "commonCatalog";

        public SubMenu getCategory(string path)
        {
            foreach (var key in items.Keys)
            {
                if (items[key].items.Any(v => path.Contains(v.item)))
                    return new SubMenu { category = key.category_tree.items[0], isCategory = true};
            }
            return null;
        }

        internal void tryLoad()
        {
          items = TextUtils.tryRestore<Dictionary<Category, LibItems>>(commonCat);
            if (items == null || items.Any(i => i.Key.category_tree == null))
                items = new Dictionary<Category, LibItems>();
           var local = TextUtils.tryRestore<Dictionary<Category, LibItems>>(catalogFile);
            if (local.All(l => l.Key.category_tree != null))
                items.MergeDictionaries(local);
        }

        public void addCategory(string text, bool isSub = false,string parent = null)
        {
            if (isSub)
                items.Add(new Category(parent,text),new LibItems());
            items.Add(new Category(text), new LibItems());
        }

        public string assignFolder(object selectedItem1, object selectedItem2,int deep)
        {
            var path = selectedItem1 as string;
            var ccateg = selectedItem2 as string;
          
            var subFld = path.Split('\\');
            if (subFld.Length <= 1)
                return null;
            string item = subFld[1];
            if (subFld.Length > 3 && deep == 2) item = $"{subFld[1]}\\{subFld[2]}";
            addItem(ccateg ?? item, item);
            return item;
        }

        List<CatalogItem> inCategory(string categ)
        {/*
 var key = items.Keys.SingleOrDefault(k => k.category_tree.items[1] == categ);
            if (key != null)
            {
                key = new Category(categ);
                items.Add(key, new LibItems());
            }*/
            var key = items.Keys.SingleOrDefault(k => k.category_tree.items[0] == categ);
            if (key == null)
            {
                key = new Category(categ);
                items.Add(key, new LibItems());
            }
            return items[key].items;
        }
        private void addItem(string ccateg, string item)
        {
           inCategory(ccateg).Add(new CatalogItem(item));
        }

        public void saveCatalog()
        {
            TextUtils.save(items, catalogFile);
        }

        public string[] getAssigned(object selectedCateg)
        {
           return inCategory(selectedCateg as string).Select(i => i.item).ToArray();
        }
    }
[Serializable]
    public class LibItems
    {
        public List<CatalogItem> items { get; set; } = new List<CatalogItem>();
    }
[Serializable]
    public class CatalogItem
    {
        public string item { get; set; }
        public bool isExe;

        public CatalogItem(string item)
        {
            this.item = item;
        }
    }

    [Serializable]
    public class Category
    {
        public CategTree category_tree { get; set; } = new CategTree();
        

        public Category(string text, string subcateg = null)
        {
            category_tree.items.Add( text);
            category_tree.items.Add( subcateg);
        }

        HashSet<string> pathContains;
    }
    [Serializable]
    public class CategTree
    {
        public List<string> items = new List<string>();
        public bool isOne => count == 1;
        public int count => items.Count;
    }
    
}
