﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSBrowser
{
    [Serializable]
    public class SoftInfo
    {
        public static Dictionary<FolderDesc, List<ExeItem>> data { get; set; }
           = new Dictionary<FolderDesc, List<ExeItem>>();
        public List<ExeItem> this[string i]
        {
            get {
                return data.FirstOrDefault(k => k.Key.Name == i).Value;                
                
                
            }
           // set { arr[i] = value; }
        }
        public List<string> Folders => data?.Select(d => d.Key.Name).ToList() ?? new List<string>();
        public List<ExeItem> Exes => data.SelectMany(d => d.Value).ToList();
        internal void addExes(string subFolder, List<ExeItem> exes)
        {
            var desc = new FolderDesc(subFolder);
            if (data == null)
                data = new Dictionary<FolderDesc, List<ExeItem>>();
            if (data.Any(d => d.Key.Name == subFolder))
            {
                data[desc].AddRange(exes);
            }
            else
                data.Add(desc, exes);
        }
    }
}
