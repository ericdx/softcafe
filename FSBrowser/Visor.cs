﻿/*
 * Created by SharpDevelop.
 * User: Лилия
 * Date: 10.07.2019
 * Time: 00:27
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System;
using ClassLib;
using System.Text.RegularExpressions;
using TextLib;

namespace FSBrowser
{
    /// <summary>
    /// file operator
    /// </summary>
    public class Visor
    {
        
        //drive -> tree
        static Dictionary<string, FolderInfo> folderTree;
        public Visor()
        {
            //_start = startPath;
            folderTree = new Dictionary<string, FolderInfo>();
        }
        public void scanForNew() { }
        public List<string> restricted = new List<string>();

        public static bool stopSignal { get; set; }
        public static string unsortedFolder => "--UNSORTED--";

        string[] buildFoldersTree()
        {

            DriveInfo.GetDrives().ToList()
                .ForEach(d =>
                {
                    var driveFolders = d.RootDirectory.GetDirectories().ToList();

                    folderTree.Add(d.RootDirectory.FullName, new FolderInfo(driveFolders));

                });
            return new string[] { };
        }

        public string[] FindNewSoft(string[] rootFolders)
        {
           
            var nextFoldersScan = new List<string>();
            // Parallel.ForEach(rootFolders, _start =>
            var totalExe = int.Parse(Settings.Get("toScan")[0]);
            int exe_count = 0;
            foreach (var currentFolder in rootFolders)
            {
                var catalog = new SoftLibra(currentFolder);
                List<string> subFolders;
                try
                {
                    var dir = new DirectoryInfo(currentFolder);
                    subFolders = dir.GetDirectories().Where(
                        r => !r.Name.equalAnycaseTrim("$recycle.bin"))
                        .Select(d => d.Name).ToList();

                }
                catch (Exception)
                {
                    continue;
                }
                foreach (var subFolder in subFolders)
                {
                    var path = Path.Combine(currentFolder, subFolder);

                    try
                    {
                        var exes = Directory.GetFiles(path, "*.exe").ToList();
                        exes = exes.Where(e => !Regex.IsMatch(e, @"unins.*\.exe")).ToList();
                        if (exes.Any())
                        {
                            exe_count += exes.Count;
                            var vfolder = subFolder;
                            if (exes.Count == 1)
                                vfolder = unsortedFolder;
                            catalog.Add(vfolder, exes);
                            if (exe_count > totalExe) goto EXIT;  
                        }
                        nextFoldersScan.Add(path);
                    }
                    catch (Exception) { }

                    if (stopSignal) goto EXIT;  
                }
            }//root loop  
            EXIT:                        
            return
            (nextFoldersScan.ToArray());
        }

        public static void clear()
        {
            SoftLibra.clearMissing();
        }
    }
}