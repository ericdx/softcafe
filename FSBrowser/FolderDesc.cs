﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FSBrowser
{
    [Serializable]
    public class FolderDesc
    {
        public string Name;
        public bool isVirtual;        

        public FolderDesc(string subFolder)
        {
            Name = subFolder;
            isVirtual = false;
        }
    }
}
