﻿/*
 * Created by SharpDevelop.
 * User: Лилия
 * Date: 10.07.2019
 * Time: 00:35
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TextLib;

namespace FSBrowser
{
    /// <summary>
    /// Description of SoftLibra.
    /// </summary>
    public class SoftLibra
    {
        string root;

        public SoftLibra(string rootFolder)
        {
            root = rootFolder;
        }

        static SoftLibra()
        {
            catalog.tryLoad();

        }

        public static string[] lookup(string text)
        {
           return unsorted.Where(u => u.Contains(text)).Take(15).ToArray();
        }

        public static Catalog catalog = new Catalog(); //media -> winamp..
        /// <summary>
        /// folder info -> exe's
        /// </summary>
        public static SoftInfo allSoft { get; set; } = new SoftInfo();
           
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subFolder">short folder with exe</param>
        /// <param name="exeFullPaths"></param>
        public void Add(string subFolder, List<string> exeFullPaths)
        {
            var exes = exeFullPaths.Select(p => new ExeItem
            {
                appName = Path.GetFileNameWithoutExtension(p),

                folderFS = Path.Combine(root, subFolder)
            }).ToList();
            allSoft.addExes(subFolder,exes);
           
        }
        public static List<string> mainMenu
        {
            get
            {
                return allSoft.Folders;
            }
        }      

        public static string[] unsorted
        {
            get
            {
                return allSoft.Exes.Where(c => !c.catalogOrFolder.isCategory).Select(s => s.AppPath).ToArray();
            }
        }
        static string data_f = "data1";
        public static bool tryRestore()
        {
            SoftInfo.data = TextUtils.load(data_f)
                as Dictionary<FolderDesc, List<ExeItem>>;

            if (//restoredSoft != null && 
                allSoft.Folders?.Any() ?? false)
            {
               // allSoft = restoredSoft;

                return true;
            }
            return false;

        }
        public static void flush()
        {
            TextUtils.save(SoftInfo.data, data_f);
        }

        public static object[] getCategories()
        {
            return catalog.items.Select(c => c.Key.category_tree.items[0]).ToArray();
        }

        internal static void clearMissing()
        {

           /* var newDict = new Dictionary<string, List<ExeItem>>();
           foreach (var dir in allSoft.Folders)
            {
                newDict.Add(dir,allSoft[dir].Where(w => w.exist_Exe).ToList());
            }
            allSoft = newDict;*/
        }
    }
    [Serializable]
    public class ExeItem
    {
        public string appName; //git - '.exe'

        public SubMenu catalogOrFolder
        {
            get
            {
                return SoftLibra.catalog.getCategory(folderFS) ?? new SubMenu {category = folderFS, isCategory = false}; 
            }
        }

        public string folderFS { get; set; }//folder with exe
                                                //public string parentFolder { get; set; }
                                                //icon
        public string vpath; //develop/version control/
        public string AppPath
        {
            get { return folderFS + "\\" + appName; }
        }
        public string fullExePath
        {
            get { return Path.Combine(folderFS, appName + ".exe"); }
        }

        public bool exist_Exe => File.Exists(fullExePath);
    }

    [Serializable]
    public class SubMenu
    {
        public string category  { get; set; }
        public bool isCategory  { get; set; }
    }
}
